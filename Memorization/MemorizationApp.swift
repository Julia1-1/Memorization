//
//  MemorizationApp.swift
//  Memorization
//
//  Created by Julia Kozłowska on 21/03/2024.
//

import SwiftUI

@main
struct MemorizationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
