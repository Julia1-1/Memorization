//
//  ContentView.swift
//  Memorization
//
//  Created by Julia Kozłowska on 21/03/2024.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HStack() {
            CardView(isFaceUp: true)
            CardView()
        }
        .padding()
        .foregroundColor(.orange)
    }
}


struct CardView: View {
    var isFaceUp: Bool = false
    var body: some View {
        ZStack() {
            if (isFaceUp) {
                RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/)
                    .foregroundColor(.white)
                RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/)
                     .strokeBorder(lineWidth: 4)
                Text("Mem🧠rize")
                    .font(.subheadline)
                    .foregroundColor(.indigo)
            } else {
                RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/)
            }

        }
    } 
}




#Preview {
    ContentView()
}
